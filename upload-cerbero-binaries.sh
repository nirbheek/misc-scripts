#!/bin/bash
#
# vim: set sts=4 sw=4 et :

set -e

GSTTMPDIR="/srv/gstreamer.freedesktop.org/scratch/gst"
SERVER="gstreamer.freedesktop.org"
ROOTDIR="/srv/gstreamer.freedesktop.org/www/data/pkg"
SSH_CONN_PID=0
PARALLEL_UPLOAD=${PARALLEL_UPLOAD:-4}

#SCP="scp -p"
SCP="rsync -av --progress --append-verify"
SSH="ssh -o ControlPath=$HOME/.ssh/ssh-%r@%h:%p"
KILL=kill
MV=mv
if [[ -n $DRY_RUN ]]; then
    SCP="echo $SCP"
    SSH="echo $SSH"
    KILL="echo $KILL"
    MV="echo $MV"
fi

ARGS=()
for arg in "$@"; do
    [[ -d $arg ]] && continue
    ARGS+=($arg)
done

if [[ -z "${ARGS[@]}" ]]; then
    echo "Usage: $0 <files to upload>"
    exit 1
fi

get_subdir_for_file() {
    if [[ $1 =~ (mingw|msvc|uwp) ]]; then
        echo windows
    elif [[ $1 =~ android ]]; then
        echo android
    elif [[ $1 =~ ios-universal\.pkg ]]; then
        echo ios
    elif [[ $1 =~ (x86_64|universal)\.pkg ]]; then
        echo osx
    elif [[ $1 =~ cerbero-1\..*\.tar\.(xz|gz) ]]; then
        echo src
    fi
}

get_version_for_file() {
    sed -n -e 's/.*\(1\.[0-9]\+\.[0-9]\+\).*/\1/p' <<<"$1"
}

kill_ssh_conn() {
    $KILL $SSH_CONN_PID
}

check_have_disk_space() {
    local need=$(du -c "$@" | tail -1 | cut -f1)
    local avail=$($SSH "$SERVER" df --output=avail "$ROOTDIR" | tail -1)

    # Require 20% extra up to 1GB so we don't consume all the space on the
    # server, and also to account for other people using space on the server
    # while we're uploading.
    extra=$((need / 5))
    gb_1=$((1024 * 1024))
    [[ $extra -gt $gb_1 ]] && extra=$gb_1
    need=$((need + extra))

    if [[ $need -gt $avail ]]; then
        echo "Not enough disk space on server: need $((need/1024))MB but have $((avail/1024))MB"
        exit 1
    fi
}

fix_perms() {
    local wantgrp='gstreamer'
    local owner=$($SSH "$SERVER" stat -c %U "'${1}'")
    local myuser=$($SSH "$SERVER" whoami)
    echo "> Checking/fixing permissions for $1"
    if [[ $owner == $myuser ]]; then
        $SSH "$SERVER" chgrp $wantgrp "'${1}'"
        $SSH "$SERVER" chmod g+rw,o+r "'${1}'"
    else
        local curgrp=$($SSH "$SERVER" stat -c %G "'${1}'")
        local curmode=$($SSH "$SERVER" stat -c %a "'${1}'")
        if [[ $curgrp != $wantgrp ]] && [[ -z $DRY_RUN ]]; then
            echo "WARNING: Group of '$1' is $curgrp instead of $wantgrp, ask $owner to fix"
        fi
        if [[ $curmode != 2775 ]] && [[ $curmode != 644 ]] && [[ -z $DRY_RUN ]]; then
            echo "WARNING: Mode of '$1' is $curmode instead of 2775 or 664, ask $owner to fix"
        fi
    fi
}

upload_file() {
    local version="$(get_version_for_file "$1")"
    local location="$ROOTDIR/$(get_subdir_for_file "$1")/$version"
    local subdirs=($location)
    if ! [[ $version =~ 1\.16\.? ]]; then
        if [[ $1 =~ msvc ]]; then
            location="$location/msvc"
            subdirs+=($location)
        elif [[ $1 =~ mingw ]]; then
            location="$location/mingw"
            subdirs+=($location)
        elif [[ $1 =~ uwp ]]; then
            location="$location/uwp"
            subdirs+=($location)
        fi
    fi
    $SCP "$1" "$SERVER:$GSTTMPDIR"
    $SSH "$SERVER" mkdir -p "'${location}'"
    for subdir in "${subdirs[@]}"; do
        fix_perms "${subdir}"
    done
    $SSH "$SERVER" mv -n "$GSTTMPDIR/$(basename "$1")" "'${location}/'"
    fix_perms "${location}/$(basename "$1")"
    mkdir -p "$(dirname "$1")/done"
    $MV -v "$1" "$(dirname "$1")/done"
}

for i in "${ARGS[@]}"; do
    version=$(get_version_for_file "$i")
    if [[ -z $version ]]; then
        echo "Could not detect version from file $i"
        exit 1
    fi
    subdir=$(get_subdir_for_file "$i")
    if [[ -z $subdir ]]; then
        echo "Could not detect subdir for file $i"
        exit 1
    fi
done

$SSH -M -N "$SERVER" &
SSH_CONN_PID=$!

trap kill_ssh_conn EXIT ERR INT TERM

check_have_disk_space "${ARGS[@]}"

queued=0
for i in "${ARGS[@]}"; do
    echo "Uploading $i"
    if (( PARALLEL_UPLOAD == 1 )); then
        upload_file "$i"
    else
        upload_file "$i" &
        queued=$((queued+1))
        echo ">>> Queued $queued/$# uploads"
        while (( $(jobs | wc -l) > PARALLEL_UPLOAD )); do
            sleep 1
        done
    fi
done

echo "Waiting for upload jobs to complete..."
while (( $(jobs | wc -l) > 1 )); do
    jobs
    sleep 1
done
