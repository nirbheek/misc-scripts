#!/usr/bin/env python3
# vim: set sts=4 sw=4 et tw=0 :

import re
import os
import sys
import shutil
import subprocess

ninja = shutil.which('ninja') or shutil.which('ninja-build')

if not ninja:
    print('Unable to find ninja. Please install it.')
    sys.exit(1)

build_ninja = None
build_dir = None
if len(sys.argv) == 2:
    if sys.argv[1].endswith('build.ninja') and os.path.isfile(sys.argv[1]):
        build_ninja = sys.argv[1]
        build_dir = os.path.dirname(sys.argv[1])
    else:
        f = os.path.join(sys.argv[1], 'build.ninja')
        if os.path.isfile(f):
            build_ninja = f
            build_dir = sys.argv[1]

if not build_ninja:
    print('Usage: {} <path/to/build.ninja>'.format(sys.argv[0]))
    sys.exit(1)

# Rules that don't involve building anything
rules_ignored = ('SHSYM', 'REGENERATE_BUILD')

rules = []
targets = []
build_regex = re.compile(r'^build (?P<target>[^:]+): (?P<rule>[^\s]+) (?P<deps>.*)')
with open(build_ninja) as f:
    for line in f:
        if line.startswith('rule '):
            rule = line.split('rule ')[1][:-1]
            if rule not in rules_ignored:
                rules.append(rule)
        if line.startswith('build '):
            res = build_regex.search(line)
            if not res:
                continue
            target_deps = res.group('deps')
            target_rule = res.group('rule')
            # Ignore targets that have no dependencies because there's no
            # dependency to be checked there and ignore targets that depend on
            # the PHONY target because those will always be run
            if target_deps and 'PHONY' not in target_deps and \
               target_rule not in rules_ignored:
                targets.append(res.group('target'))

failed = []
index = 0
n_targets = len(targets)
for target in targets:
    index += 1
    print('Building target [{}/{}] {}'.format(index, n_targets, target))
    ret = subprocess.run([ninja, '-v', '-C', build_dir, target], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    if ret.returncode != 0:
        print('FAILED:')
        print(ret.stdout)
        failed.append(ret)
    subprocess.run([ninja, '-C', build_dir, 'clean'], stdout=subprocess.PIPE, universal_newlines=True)
    # Don't store header dependency information retrieved from the compiler.
    # This hides dependency issues in the build file itself because it gets
    # supplemented by what the compiler tells Ninja and is used for further
    # Ninja invocations. See: https://ninja-build.org/manual.html#ref_headers
    os.remove(os.path.join(build_dir, '.ninja_deps'))

if failed:
    print("The following targets failed to build:")
for each in failed:
    print(each.args)
    print("Return code: {}".format(each.returncode))
