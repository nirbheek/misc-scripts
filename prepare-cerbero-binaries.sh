#!/bin/bash
#
# vim: set sts=4 sw=4 et :

set -e

for i in "$@"; do
    [[ -d $i ]] && continue
    D=$(dirname $i)
    F=$(basename $i)
    cd "$D"
    sha256sum "$F" > "$F.sha256sum"
    echo Writing $F.asc
    gpg2 --detach-sign --armor -o "$F.asc" "$F"
    cd - &>/dev/null
done
