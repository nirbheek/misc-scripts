#!/bin/bash
# vim: set sts=4 sw=4 et :

set -ex

./cerbero-uninstalled -c config/win64.cbc wipe --keep-sources --force
while ! [[ -f BOOTSTRAP_DONE ]]; do echo "Waiting for bootstrap..."; sleep 1; done
echo "Starting win64 vs"
./cerbero-uninstalled -c config/win64.cbc package gstreamer-1.0 --offline -b
echo > WIN64_VS_DONE
