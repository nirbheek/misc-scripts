#!/bin/bash
# vim: set sts=4 sw=4 et :

set -ex

arch=$1

if ! [[ $arch =~ 32\|64 ]]; then
    echo "Usage: $0 <32|64>"
fi

./cerbero-uninstalled -c config/win${arch}.cbc -v mingw wipe --keep-sources --force
while ! [[ -f BOOTSTRAP_DONE ]]; do echo "Waiting for bootstrap..."; sleep 1; done
rm -f WIN${arch}_MINGW_DONE
echo "Starting win$arch mingw"
./cerbero-uninstalled -c config/win$arch.cbc -v mingw package gstreamer-1.0 --offline -b
echo > WIN${arch}_MINGW_DONE
