#!/bin/bash
# vim: set sts=4 sw=4 et :

set -ex

./cerbero-uninstalled -c config/win32.cbc wipe --keep-sources --force
while ! [[ -f BOOTSTRAP_DONE ]]; do echo "Waiting for bootstrap..."; sleep 1; done
rm -f WIN32_VS_DONE
echo "Starting win32 vs"
./cerbero-uninstalled -c config/win32.cbc -v norust package gstreamer-1.0 --offline -b
while ! [[ -f WIN64_VS_DONE ]]; do echo "Waiting for win64 vs..."; sleep 1; done
./cerbero-uninstalled -c config/win64.cbc package gstreamer-1.0 --offline -b
echo > WIN32_VS_DONE
