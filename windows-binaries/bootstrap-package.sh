#!/bin/bash
# vim: set sts=4 sw=4 et :

set -ex

rm -f *_DONE
./cerbero-uninstalled fetch-bootstrap --jobs=8
./cerbero-uninstalled fetch-package gstreamer-1.0 --jobs=8
./cerbero-uninstalled fetch-package gstreamer-1.0 --jobs=8
./cerbero-uninstalled wipe --force --keep-sources --build-tools
./cerbero-uninstalled bootstrap --offline
./cerbero-uninstalled -c config/cross-uwp-universal.cbc bootstrap --offline
./cerbero-uninstalled -c config/win64.cbc bootstrap --system=no --build-tools=no --offline
./cerbero-uninstalled -c config/win32.cbc bootstrap --system=no --build-tools=no --offline
rm -f gst*.msi gst*-merge-modules.zip *.wxs *.wxobj gst*uwp*.tar.xz
echo > BOOTSTRAP_DONE

while ! [[ -f WIN64_MINGW_DONE ]] && ! [[ -f WIN32_MINGW_DONE ]] && ! [[ -f WIN64_VS_DONE ]] && ! [[ -f WIN32_VS_DONE ]]; do
    echo "Waiting for package build..."; sleep 1
done
echo "Starting MSI packaging"

while true; do
    if [[ -f WIN64_VS_DONE ]] && ! [[ -f WIN64_VS_PKG_DONE ]]; then
        ./cerbero-uninstalled -c config/win64.cbc package gstreamer-1.0 --offline && echo > WIN64_VS_PKG_DONE
    fi
    if [[ -f WIN32_VS_DONE ]] && ! [[ -f WIN32_VS_PKG_DONE ]]; then
        ./cerbero-uninstalled -c config/win32.cbc package gstreamer-1.0 --offline && echo > WIN32_VS_PKG_DONE
    fi
    if [[ -f WIN64_MINGW_DONE ]] && ! [[ -f WIN64_MINGW_PKG_DONE ]]; then
        ./cerbero-uninstalled -c config/win64.cbc -v mingw package gstreamer-1.0 --offline && echo > WIN64_MINGW_PKG_DONE
    fi
    if [[ -f WIN32_MINGW_DONE ]] && ! [[ -f WIN32_MINGW_PKG_DONE ]]; then
        ./cerbero-uninstalled -c config/win32.cbc -v mingw package gstreamer-1.0 --offline && echo > WIN32_MINGW_PKG_DONE
    fi
    if [[ -f WIN64_VS_PKG_DONE ]] && [[ -f WIN32_VS_PKG_DONE ]] && [[ -f WIN32_MINGW_PKG_DONE ]] && [[ -f WIN64_MINGW_PKG_DONE ]]; then
        echo "All done"
        break
    fi
    echo "Waiting for build"; sleep 1
done
