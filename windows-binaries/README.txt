Run this in a terminal:
=======================

./bootstrap-package.sh

Then, run in separate terminals:
================================

./build-vs64.sh

./build-vs32.sh

./build-mingw.sh 32

./build-mingw.sh 64

#./cerbero-uninstalled -c config/cross-uwp-universal.cbc wipe --keep-sources --force && \
#    while ! [[ -f BOOTSTRAP_DONE ]]; do echo "Waiting for bootstrap..."; sleep 1; done && \
#    echo "Starting UWP" && \
#    ./cerbero-uninstalled -c config/cross-uwp-universal.cbc package gstreamer-1.0 --offline --jobs=8

#./cerbero-uninstalled -c config/cross-uwp-universal.cbc -v vscrt=mdd wipe --keep-sources --force && \
#    while ! [[ -f BOOTSTRAP_DONE ]]; do echo "Waiting for bootstrap..."; sleep 1; done && \
#    echo "Starting UWP debug" && \
#    ./cerbero-uninstalled -c config/cross-uwp-universal.cbc -v vscrt=mdd package gstreamer-1.0 --offline --jobs=8

Known Issues:
=============
- cerbero extract doesn't lock correctly, so is racy
