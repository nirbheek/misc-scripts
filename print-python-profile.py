#!/usr/bin/env python3
import sys
import pstats

p = pstats.Stats(sys.argv[1])
p.strip_dirs().sort_stats('cumtime').print_stats(100)
#p.print_callers("get_target_dir")
